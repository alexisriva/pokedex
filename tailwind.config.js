module.exports = {
  purge: [
    './src/pages/**/*.{js,ts,jsx,tsx}',
    './src/components/**/*.{js,ts,jsx,tsx}',
  ],
  darkMode: false, // or 'media' or 'class'
  theme: {
    extend: {
      colors: {
        pearl: '#082032',
        'pickled-bluewood': '#334756',
        'alizarin-crimson': '#DF2E2E',
        mercury: '#E8E8E8',
      },
    },
  },
  variants: {
    extend: {},
  },
  plugins: [],
};
