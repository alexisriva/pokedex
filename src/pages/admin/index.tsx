import React from 'react';
import Head from 'next/head';
import { useRouter } from 'next/router';
import Button from '../../components/Button';

const Admin: React.FC = () => {
  const router = useRouter();

  return (
    <>
      <Head>
        <title>Admin</title>
        <meta name='description' content='Pokedex created by AR - Admin Page' />
        <link rel='icon' href='/pokeball.png' />
      </Head>

      <div className='flex flex-col items-start'>
        <h1 className='text-mercury'>Admin</h1>
        <Button
          label='Add new pokémon'
          onClick={() => router.push('admin/new')}
        />
      </div>
    </>
  );
};

export default Admin;
