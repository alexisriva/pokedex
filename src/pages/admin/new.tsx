import React from 'react';
import Head from 'next/head';
import NewPokemonForm from '../../components/NewPokemonForm';

const NewPokemon: React.FC = () => (
  <>
    <Head>
      <title>Add new pokémon</title>
      <meta name='description' content='Pokedex created by AR - Admin Page' />
      <link rel='icon' href='/pokeball.png' />
    </Head>
    <NewPokemonForm />
  </>
);

export default NewPokemon;
