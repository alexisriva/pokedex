import React from 'react';
import Head from 'next/head';
import Header from '../components/Header';
import Main from '../components/Main';
import Filters from '../components/Filters';

const Home: React.FC = () => (
  <>
    <Head>
      <title>Pokédex</title>
      <meta name='description' content='Pokedex created by AR' />
      <link rel='icon' href='/pokeball.png' />
    </Head>

    <Header />
    <Filters />
    <Main />
  </>
);

export default Home;
