import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { RootState } from '../reducers';
import { Pokemon } from './pokemon';

interface Pagination {
  count: number;
  next?: string;
  previous?: string;
  results: Pokemon[];
}

interface PaginationState {
  pagination?: Pagination;
}

const initialState: PaginationState = {
  pagination: undefined,
};

export const paginationSlice = createSlice({
  name: 'pagination',
  initialState,
  reducers: {
    update: (state, action: PayloadAction<Pagination>) => {
      state.pagination = action.payload;
    },
  },
});

export const { update } = paginationSlice.actions;

export const selectNextPage = (state: RootState) =>
  state.pagination.pagination?.next;
export const selectPreviousPage = (state: RootState) =>
  state.pagination.pagination?.previous;
export const selectPokemonList = (state: RootState) =>
  state.pagination.pagination?.results;
export default paginationSlice.reducer;
