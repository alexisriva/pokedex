import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { RootState } from '../reducers';
import { Type } from './pokemon';

interface FiltersState {
  selectedType?: Type;
  selectedGen?: number;
}

const initialState: FiltersState = {
  selectedType: undefined,
  selectedGen: undefined,
};

export const filtersSlice = createSlice({
  name: 'filters',
  initialState,
  reducers: {
    updateSelectedType: (state, action: PayloadAction<Type>) => {
      state.selectedType = action.payload;
    },
    updateSelectedGen: (state, action: PayloadAction<number>) => {
      state.selectedGen = action.payload;
    },
  },
});

export const { updateSelectedType, updateSelectedGen } = filtersSlice.actions;

export const selectSelectedType = (state: RootState) =>
  state.filters.selectedType;
export const selectSelectedGen = (state: RootState) =>
  state.filters.selectedGen;
export default filtersSlice.reducer;
