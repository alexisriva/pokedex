import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { RootState } from '../reducers';

export interface Type {
  name: string;
  color: string;
}

interface TypeInfo {
  type: Type;
  primary: boolean;
}

export interface BasePokemon {
  id: number;
  name: string;
  sprite: string;
}

interface Evolution {
  pokemon: BasePokemon;
  evolves_to?: Evolution;
}

export interface EvolutionChain {
  chain: Evolution;
}

export interface Pokemon extends BasePokemon {
  types: TypeInfo[];
  pokedex_entry?: string;
  generation?: number;
  evolution_chain?: EvolutionChain;
}

interface PokemonState {
  pokemon?: Pokemon;
}

const initialState: PokemonState = {
  pokemon: undefined,
};

export const pokemonSlice = createSlice({
  name: 'pokemon',
  initialState,
  reducers: {
    update: (state, action: PayloadAction<Pokemon>) => {
      state.pokemon = action.payload;
    },
  },
});

export const { update } = pokemonSlice.actions;

export const selectPokemon = (state: RootState) => state.pokemon.pokemon;
export default pokemonSlice.reducer;
