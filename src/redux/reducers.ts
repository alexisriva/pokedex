import { combineReducers } from 'redux';
import pokemon from './slices/pokemon';
import pagination from './slices/pagination';
import filters from './slices/filters';

const rootReducer = combineReducers({ pokemon, pagination, filters });

export type RootState = ReturnType<typeof rootReducer>;

export default rootReducer;
