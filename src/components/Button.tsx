import React from 'react';

export interface Props {
  label: string;
  disabled?: boolean;
  onClick: () => void;
}

const Button: React.FC<Props> = ({ label, disabled = false, onClick }) => (
  <div
    className={`${
      disabled
        ? 'bg-gray-600 text-gray-500 cursor-not-allowed'
        : 'bg-alizarin-crimson text-mercury cursor-pointer'
    } rounded-lg text-center py-2 px-4 flex-1 select-none`}
    onClick={onClick}
  >
    {label}
  </div>
);

export default Button;
