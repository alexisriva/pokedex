import React from 'react';

const Header: React.FC = () => (
  <div className='flex flex-col text-center text-mercury pt-3'>
    <h1 className='text-2xl font-bold'>Pokédex</h1>
    <h4>Created by: Alexis Rivadeneira</h4>
  </div>
);

export default Header;
