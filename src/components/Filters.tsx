import React, { useEffect, useState } from 'react';
import { useDispatch } from 'react-redux';
import { updateSelectedType, updateSelectedGen } from '../redux/slices/filters';
import { Type } from '../redux/slices/pokemon';
import PokeTypeTag from './PokeTypeTag';
import { types as typesUrl } from '../utils/urls';

const Filters: React.FC = () => {
  const dispatch = useDispatch();

  const [selectedTypeIndex, setSelectedTypeIndex] = useState(-1);
  const [types, setTypes] = useState<Type[]>([]);

  const [selectedGenIndex, setSelectedGenIndex] = useState(-1);

  const requestTypesData = () => {
    fetch(typesUrl)
      .then((res) => res.json())
      .then((res) => setTypes(res));
  };

  const handleTypeSelection = (type: Type, index: number) => {
    dispatch({ type: updateSelectedType, payload: type });
    setSelectedTypeIndex(index);
  };

  const handleGenSelection = (gen: number) => {
    dispatch({ type: updateSelectedGen, payload: gen + 1 });
    setSelectedGenIndex(gen);
  };

  const handleClearFilters = () => {
    dispatch({ type: updateSelectedType, payload: undefined });
    dispatch({ type: updateSelectedGen, payload: undefined });
    setSelectedTypeIndex(-1);
    setSelectedGenIndex(-1);
  };

  useEffect(() => {
    requestTypesData();
  }, []);

  return (
    <div className='flex flex-col justify-center gap-4 px-12 py-5'>
      <div className='flex flex-wrap gap-4'>
        {types.map((type, index) => (
          <div
            key={index}
            className='cursor-pointer'
            onClick={() => handleTypeSelection(type, index)}
          >
            <PokeTypeTag
              name={type.name}
              color={type.color}
              selected={selectedTypeIndex === index}
            />
          </div>
        ))}
      </div>
      <div className='flex flex-wrap gap-4'>
        {Array.from(Array(8).keys()).map((gen) => (
          <div
            key={gen}
            className='cursor-pointer'
            onClick={() => handleGenSelection(gen)}
          >
            <PokeTypeTag
              name={`Gen ${gen + 1}`}
              color='#DF2E2E'
              selected={selectedGenIndex === gen}
            />
          </div>
        ))}
      </div>
      <div
        className='bg-alizarin-crimson text-mercury rounded-lg text-center py-1 px-2 select-none cursor-pointer self-start'
        onClick={() => handleClearFilters()}
      >
        Clear filters
      </div>
    </div>
  );
};

export default Filters;
