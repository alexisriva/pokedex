import React from 'react';
import { Type } from '../redux/slices/pokemon';

interface TypeTag extends Type {
  selected?: boolean;
}

const PokeTypeTag: React.FC<TypeTag> = ({ name, color, selected = false }) => (
  <div
    className='border rounded-lg px-2 py-1 text-xs uppercase'
    style={{
      borderColor: color,
      color: selected ? 'white' : color,
      backgroundColor: selected ? color : 'transparent',
    }}
  >
    {name}
  </div>
);

export default PokeTypeTag;
