import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { selectSelectedType, selectSelectedGen } from '../redux/slices/filters';
import {
  selectNextPage,
  selectPokemonList,
  selectPreviousPage,
  update,
} from '../redux/slices/pagination';
import PokemonListItem from './PokemonListItem';
import Button from './Button';
import { pokemon } from '../utils/urls';

const PokemonList: React.FC = () => {
  const dispatch = useDispatch();

  const selectedType = useSelector(selectSelectedType);
  const selectedGen = useSelector(selectSelectedGen);

  const pokemonList = useSelector(selectPokemonList);
  const nextPage = useSelector(selectNextPage);
  const previousPage = useSelector(selectPreviousPage);

  const requestPokeData = (url: string) => {
    fetch(url)
      .then((res) => res.json())
      .then((res) => dispatch({ type: update, payload: res }));
  };

  useEffect(() => {
    let url = pokemon;
    if (selectedType != undefined) {
      url += `?type=${selectedType.name}`;
    }

    if (selectedGen != undefined) {
      url += `?gen=${selectedGen}`;
    }

    requestPokeData(url);
  }, [selectedType, selectedGen]);

  const requestNextPage = () => {
    if (nextPage) {
      requestPokeData(nextPage);
    }
  };

  const requestPreviousPage = () => {
    if (previousPage) {
      requestPokeData(previousPage);
    }
  };

  return (
    <div className='flex flex-col gap-5'>
      <div className='grid grid-cols-3 gap-3'>
        {pokemonList?.map((pokemon) => (
          <PokemonListItem key={pokemon.id} {...pokemon} />
        ))}
      </div>
      <div className='flex gap-4'>
        <Button
          label='Previous'
          disabled={previousPage == undefined}
          onClick={requestPreviousPage}
        />
        <Button
          label='Next'
          disabled={nextPage == undefined}
          onClick={requestNextPage}
        />
      </div>
    </div>
  );
};

export default PokemonList;
