import React from 'react';
import Image from 'next/image';
import { useDispatch } from 'react-redux';
import { Pokemon, update } from '../redux/slices/pokemon';
import PokeTypeTag from './PokeTypeTag';
import { pokemon as pokemonAPI } from '../utils/urls';

const PokemonListItem: React.FC<Pokemon> = (pokemon) => {
  const dispatch = useDispatch();

  const updatePokemon = () => {
    fetch(`${pokemonAPI}${pokemon?.id}`)
      .then((res) => res.json())
      .then((res) => {
        dispatch({ type: update, payload: res });
      });
  };

  return (
    <div
      className='flex gap-2 items-center p-3 border border-mercury rounded-2xl text-mercury text-xl cursor-pointer'
      onClick={updatePokemon}
    >
      <div className='bg-pickled-bluewood rounded-full flex items-center'>
        <Image
          src={pokemon.sprite}
          alt={`${pokemon.name}-sprite`}
          width={90}
          height={90}
        />
      </div>
      <div className='flex flex-col'>
        <p className='text-sm font-bold'>{`# ${pokemon.id}`}</p>
        <p>{pokemon.name}</p>
        <div className='flex gap-2 mt-2'>
          {pokemon.types.map((value, index) => (
            <PokeTypeTag key={index} {...value.type} />
          ))}
        </div>
      </div>
    </div>
  );
};

export default PokemonListItem;
