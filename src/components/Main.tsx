import React from 'react';
import PokemonDetail from './PokemonDetail';
import PokemonList from './PokemonList';

const Main: React.FC = () => (
  <div className='flex justify-center gap-8 px-12 py-5'>
    <div className='w-3/5'>
      <PokemonList />
    </div>
    <div className='w-2/5'>
      <PokemonDetail />
    </div>
  </div>
);

export default Main;
