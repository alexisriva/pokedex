import React from 'react';
import Image from 'next/image';
import { BasePokemon } from '../redux/slices/pokemon';

const PokeTile: React.FC<BasePokemon> = ({ name, sprite }) => (
  <div className='flex flex-col bg-mercury rounded-lg p-2'>
    <Image src={sprite} alt={`${name}-sprite`} width={75} height={75} />
    <p className='text-center text-pearl'>{name}</p>
  </div>
);

export default PokeTile;
