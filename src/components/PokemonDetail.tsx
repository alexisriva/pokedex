import Image from 'next/image';
import React from 'react';
import { useSelector } from 'react-redux';
import { selectPokemon } from '../redux/slices/pokemon';
import PokeEvolutions from './PokeEvolutions';
import PokeTypeTag from './PokeTypeTag';

const PokemonDetail: React.FC = () => {
  const pokemon = useSelector(selectPokemon);
  const color = pokemon?.types[0].type.color;

  return (
    <>
      {pokemon && (
        <div
          className='flex flex-col p-4 rounded-2xl border'
          style={{ borderColor: color, color: color }}
        >
          <div className='flex justify-between items-center'>
            <p className='text-xl font-bold'>
              {pokemon.name}{' '}
              <span className='font-normal'>- Gen {pokemon.generation}</span>
            </p>
            <p>{`# ${pokemon.id}`}</p>
          </div>
          <div className='flex'>
            <Image
              src={pokemon.sprite}
              alt={`${pokemon.name}-sprite`}
              width={200}
              height={200}
            />
            <div className='flex flex-col text-mercury flex-1 py-1'>
              <div className='flex items-center gap-2 mb-3'>
                <span className='text-xs'>Types:</span>
                {pokemon.types.map((value, index) => (
                  <PokeTypeTag key={index} {...value.type} />
                ))}
              </div>
              <p className='text-xs'>Pokédex entry</p>
              <div className='border border-mercury p-2 rounded-lg'>
                {pokemon.pokedex_entry}
              </div>
            </div>
          </div>
          <p className='font-bold mt-4'>Evolution Chain:</p>
          {pokemon.evolution_chain?.chain && (
            <div className='flex items-center justify-center'>
              <PokeEvolutions chain={pokemon.evolution_chain?.chain} />
            </div>
          )}
        </div>
      )}
    </>
  );
};

export default PokemonDetail;
