import React from 'react';
import Image from 'next/image';
import { EvolutionChain } from '../redux/slices/pokemon';
import PokeTile from './PokeTile';

const PokeEvolutions: React.FC<EvolutionChain> = ({ chain }) => {
  const firstEvolution = chain.evolves_to?.pokemon;
  const secondEvolution = chain.evolves_to?.evolves_to?.pokemon;
  return (
    <div className='flex gap-3'>
      <PokeTile {...chain.pokemon} />
      {firstEvolution && (
        <div className='flex gap-3'>
          <Image src='/right-arrow.svg' width={12} height={12} />
          <PokeTile {...firstEvolution} />
        </div>
      )}
      {secondEvolution && (
        <div className='flex gap-3'>
          <Image src='/right-arrow.svg' width={12} height={12} />
          <PokeTile {...secondEvolution} />
        </div>
      )}
    </div>
  );
};

export default PokeEvolutions;
