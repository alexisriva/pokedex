const base = 'http://127.0.0.1:8000';
const api = `${base}/api`;
const pokemon = `${api}/pokemon/`;

const types = `${api}/types/`;

export { pokemon, types };
